var cue = new Vue({
  el: "#app",
  data: {
    arrivalRate: 30,
    LengthOfService: 3,
    duration: 1000,
    count: 0,
    variation: {},
    customer: {},
    queue: 0,
  },
  methods: {
      execute: function() {
          this.count = 0;
          if (this.duration>100000) this.duration = 100000;
          for (i = 0; i<100; i++) {
              this.variation[i] = 0;
          }

          for (i = 0; i<10; i++) {
              this.customer[i] = 0;
          }

          for (var i = 0; i<this.duration; i++) {
              random = Math.random()*100;
              this.variation[Math.floor(random)]++;
              if (random<parseInt(this.arrivalRate)) {
                  this.count++;
                  this.queue++;
              }

              if (parseInt(this.queue)>0) {
                  if (Math.random()*10<parseInt(this.queue)) {
                      this.queue--;
                  }
              }
              this.customer[this.queue]++;
          }
      },

      color: function(key) {
            if(parseInt(this.arrivalRate)>parseInt(key)) return "in"
            return "out"
      },

      expected: function(value) {
          expection = this.duration/100;
          return value/expection*100;
      },

      customerColor: function(key) {
          if (parseInt(key)%2===0) return "customer-even";
          return "customer-odd";
      },

      customerExpected: function(key) {
          max = 0;
          for (i=0; i<10; i++) {
              if (parseInt(this.customer[i])>max) max=parseInt(this.customer[i]);
          }
          return parseInt(this.customer[key])/max*100;
      },
  }
})
